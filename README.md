# Vanilla Radio

### a modular radio stream player built in plain javascript

The radio stream player consists of several stand-alone modules that could be used and rearranged independently.

The minimal codebase uses only vanilla javascript, some plain css and basic html. (The main goal of this project is to explore the possibilities/difficulties of building a modular app without any additional libraries, packages etc.)

[Here](https://niko.jovan.gitlab.io/vanilla-radio) it is live!
