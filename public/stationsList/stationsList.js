function createStationsList() {
  let stationsList = document.createElement('div');
  stationsList.classList.add('flexContainer');

  for (let stationData of stationsData) {
    stationsList.appendChild(createStationContainer(stationData));
  }

  return stationsList;
}
