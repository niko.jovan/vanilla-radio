// TODO: build a backend

var stationsData = [
  {
    stationName: 'SR P1',
    stationSrc: 'http://http-live.sr.se/p1-aac-32',
    stationImgSrc: 'https://www.liveradio.ie/files/images/105785/resized/180x172c/111.png'
  }, {
    stationName: 'SR P2',
    stationSrc: 'http://http-live.sr.se/p2-aac-32',
    stationImgSrc: 'https://www.liveradio.ie/files/images/105791/resized/180x172c/pc.png'
  }, {
    stationName: 'SR P3',
    stationSrc: 'http://http-live.sr.se/p3-aac-32',
    stationImgSrc: 'https://static.mytuner.mobi/media/radios-150px/4ZLpy5q7qQ.png'
  }, {
    stationName: 'SR P4 Göteborg',
    stationSrc: 'https://http-live.sr.se/p4goteborg-aac-32',
    stationImgSrc: 'https://www.liveradio.ie/files/images/108394/resized/180x172c/swerige.jpg'
  }, {
    stationName: 'Mix Megapol',
    stationSrc: 'https://live-bauerse-fm.sharp-stream.com/mixmegapol_instream_se_aacp',
    stationImgSrc: 'http://radioapparaten.se/logos/radio-mix-megapol-webbradio.jpg'
  }, {
    stationName: 'Rix FM',
    stationSrc: 'https://fm01-ice.stream.khz.se/fm01_mp3',
    stationImgSrc: 'https://www.phonostar.de/images/auto_created/rix_fm2184x184.png'
  }
];
