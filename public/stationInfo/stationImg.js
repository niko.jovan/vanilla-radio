function createStationImg(src) {
  const img = document.createElement('img');
  img.classList.add('stationImg');
  img.src = src;
  img.alt = '?';
  return img;
}
