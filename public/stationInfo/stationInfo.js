function createStationInfo(name, imgSrc) {
  let stationInfo = document.createElement('div');
  stationInfo.classList.add('stationInfo');

  let stationImg = createStationImg(imgSrc);
  stationInfo.appendChild(stationImg);

  let stationName = createStationName(name);
  stationInfo.appendChild(stationName);

  return stationInfo;
}
