function createStationName(name) {
  let stationName = document.createElement('div');
  stationName.classList.add('stationName');
  let stationNameText = document.createTextNode(name);
  stationName.appendChild(stationNameText);
  return stationName;
}
