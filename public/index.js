const root = document.getElementById("root");

const header = document.createElement('h1');
const headerText = document.createTextNode('Vanilla Radio');
header.appendChild(headerText);
root.appendChild(header);

const description = document.createElement('p');
const descriptionText = document.createTextNode('a modular radio stream player built in plain javascript');
description.appendChild(descriptionText);
root.appendChild(description);

root.appendChild(createStationsList());
