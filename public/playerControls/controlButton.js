function createControlButton(buttonText, clickFunction) {
  let controlButton = document.createElement('button');
  controlButton.appendChild(document.createTextNode(buttonText));
  controlButton.onclick = clickFunction;
  controlButton.classList.add('controlButton');
  return controlButton;
}
