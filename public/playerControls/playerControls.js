function createPlayerControls(src) {
  let audioSrc = src;

  let playerControls = document.createElement('div');
  playerControls.style.display = 'inline-block';

  let audioPlayer = document.createElement('audio');
  audioPlayer.id = 'audioPlayer_' + Date.now();
  playerControls.appendChild(audioPlayer);

  let startControl = createControlButton('PLAY', function() {_startPlay(audioPlayer, src)});
  playerControls.appendChild(startControl);
  let stopControl = createControlButton('STOP', function() {_stopPlay(audioPlayer)});
  playerControls.appendChild(stopControl);

  return playerControls;
}

function _startPlay(audioPlayer, audioSrc) {
  audioPlayer.volume = 0;
  audioPlayer.src = audioSrc;
  audioPlayer.load();
  audioPlayer.play();
  _fadeIn(audioPlayer);
}

function _stopPlay(audioPlayer) {
  _fadeOut(audioPlayer);
  setTimeout(function() {audioPlayer.pause(); audioPlayer.src = null;}, 500);
}

function _fadeIn(audioPlayer) {
  if (audioPlayer.volume <= 0.9) {
    audioPlayer.volume += 0.1
    setTimeout(function() {_fadeIn(audioPlayer)}, 100);
  }
}

function _fadeOut(audioPlayer) {
  if (audioPlayer.volume >= 0.1) {
    audioPlayer.volume -= 0.1
    setTimeout(function() {_fadeOut(audioPlayer)}, 50);
  }
}
