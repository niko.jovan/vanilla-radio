function createStationContainer(stationData) {
  const container = document.createElement('div');
  container.classList.add('stationContainer');
  const stationInfo = createStationInfo(stationData.stationName, stationData.stationImgSrc);
  container.appendChild(stationInfo);
  const playerControls = createPlayerControls(stationData.stationSrc);
  container.appendChild(playerControls);

  return container;
}
